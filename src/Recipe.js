import React from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Accordion from 'react-bootstrap/Accordion';


const Recipe = ({title, image,
    ingredient1, ingredient2, ingredient3, ingredient4, ingredient5, ingredient6, ingredient7,
    measure1,measure2,measure3,measure4,measure5,measure6,measure7,glass,
    instructions
}) => {
    return (
        <div className="container col-md-6">
        <div className="card my-3 container shadow p-3 mb-5 bg-white rounded">
            <h1 className='card-title mb-3 mt-3'>{title}</h1>
            <img src={image} alt="" className="img-fluid card-img"/>
            <hr />
            <h2 className="my-4">Ingredients</h2>
            <div className="text-muted">
            <p>{measure1} {ingredient1}</p>
            <p>{measure2} {ingredient2}</p>
            <p>{measure3} {ingredient3}</p>
            <p>{measure4} {ingredient4}</p>
            <p>{measure5} {ingredient5}</p>
            <p>{measure6} {ingredient6}</p>
            <p>{measure7} {ingredient7}</p>
            </div>
            <Accordion defaultActiveKey="0" className="m-3 bg-light">
            <Card>
                <Accordion.Toggle as={Button} eventKey="1" className="btn btn-light">
                    <p>Show more</p>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                <Card.Body>
                    <h2>{glass}</h2>
                    <p className='px-4 text-center text-muted'>{instructions}</p>
                </Card.Body>   
                </Accordion.Collapse>
            </Card>
            </Accordion>
        </div>
        </div>
    )
}

export default Recipe;