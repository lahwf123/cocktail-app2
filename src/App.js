import React, {useEffect, useState} from 'react';
import Recipe from './Recipe';
import './App.css';
import { render } from '@testing-library/react';

const App = () => {

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState("");
  const [query, setQuery] = useState('Martini')
  
  useEffect(() => {
      getRecipes();
  }, [query]);

  const getRecipes = async() => {
    const response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${query}`);

    const data = await response.json();

    setRecipes(data.drinks);
    console.log(data.drinks);
  };

  const updateSearch = e => {
    setSearch(e.target.value);
  };

  const getSearch = e => {
    e.preventDefault();
    setQuery(search);
    setSearch('')
  }

  return (

    <div className="App container my-4">
      <h1>Choose your poison</h1>
      <p>Search cocktail recipes and how to make them by name or type of alcohol</p>
      <form onSubmit={getSearch} className="d-flex mx-auto col-md-5 my-4">
        <input className="form-control form-control-lg" type="text" value={search} onChange={updateSearch} placeholder="Search Cocktail" />
        <button className="btn btn-light d-none" type="submit">
          Search
        </button>
      </form>
      <div className="d-flex flex-wrap">
      {recipes ? recipes.map(recipe => (
        <Recipe 
        key={recipe.idDrink}
        title={recipe.strDrink} 
        image={recipe.strDrinkThumb}
        ingredient1={recipe.strIngredient1}
        ingredient2={recipe.strIngredient2}
        ingredient3={recipe.strIngredient3}
        ingredient4={recipe.strIngredient4}
        ingredient5={recipe.strIngredient5}
        ingredient6={recipe.strIngredient6}
        ingredient7={recipe.strIngredient7}
        measure1={recipe.strMeasure1}
        measure2={recipe.strMeasure2}
        measure3={recipe.strMeasure3}
        measure4={recipe.strMeasure4}
        measure5={recipe.strMeasure5}
        measure6={recipe.strMeasure6}
        measure7={recipe.strMeasure7}
        glass={recipe.strGlass}
        instructions={recipe.strInstructions}
        />)) : <p>Sorry, No such drink</p>}
      </div>
      <footer className="float-right m-4">made with <a href="https://www.thecocktaildb.com/api.php" target="_blank">TheCocktailDB</a> and love by yours truly <a href="https://github.com/rronaliu" target="_blank" >Rron Aliu</a></footer>
    </div>
  )
}


export default App;
